const express = require("express");
const app = express();
const port = 8000;

app.get("/", (req, res) => {
  res.send("This is the sample for CICD pipeline");
});

app.listen(port, () => {
  console.log(`Your app is listening to the port : ${port}`);
});
